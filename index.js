// require directive is used to load the express module/packages.
//It allows us to access the methods and function in easily creating our server.
const express = require("express");

//This creates an express application and stores this in a constant called app.
//In layman's term, app is our server
const app = express();

//Setup for allowing the server to handle data from requests
//middleware function- this are function that provides
//middleware - allow your app to read json data.
//use-we are currently using middleware
//methods used express Js are middleware
app.use(express.json());
//For our application server to run,we  need a port to listen to
const port = 3000;

//Express has methods corresponding to each HTTP Method
// "/" corresponds with our base URI.
//Localhost:3000/
//SYntax: app.httMedthos("/endpoint", (req,res))

app.get("/", (req, res) =>{
	//res.send used  the express JS module's method to send a response back to the client.
	res.send("Hello World");
});

//This route expects to received a POST requests at the URI /endpoint "/hello"

app.post("/hello", (req,res) =>{

//req.body contains the contents/data of the request
	console.log(req.body);

	res.send(`Hello there! ${req.body.firstName} ${req.body.lastName}!`);
});

/*
SCENARIO:

		We want to create a simple users database that will perform CRUD operations based on the client request. The following routes should peform its functionality:

*/

	//mock database for our users
	let users = [
		{
			username: "jandedoe",
			password: "jane123"

		},
		{
			username: "johnsmith",
			password: "john123"
		}


	];

	//This route expects to receive a POST request at the URI "/signup"
	//This will create user object in the "users" array that mirrors a real world registration:
		//All fields are required/fill out.



	app.post("/signup", (req, res) =>{
		console.log(req.body);

		//if contents of "req.body" with property of "username" and "password" is not empty.
		if(req.body.username !== "" && req.body.password !== "" && req.body.username !== undefined && req.body.password !== undefined) {

			//to store the req.body sent via Postman in the users array we will use "push" method.
			//user object will be saved in our users arrays.
			users.push(req.body);

			res.send(`User ${req.body.username} successfully registered!`);


		}
		//if the username and password are not complete
		else{
			res.send("Please input BOTH username and password.");

		}





	});

//This route expects to receive a GET request at the URI "/users"
app.get("/users", (req, res)=>{
		res.send(users);
});


//UPDATE
//This route expects to receive a PUT request at the URI "/change-password"
//This will updated the password of a user that mathes the information provided in the client/Postman
	//we will use the username as the search property


app.put("/change-password", (req, res)=>{

	//Creates a variable to store the message to be sent back1 to the client/postman(response)
	let message;

	//Create a for loop that will loop through the elements of the user array
	for(let i=0; i < users.length; i++){

		//If the username provided in the client/Postman and username of the current element/object in the loop is the same
		if(users[i].username === req.body.username){
			//Changes the password of the user found by the loop into the password provided in the client/postman.
		
		users[i].password = req.body. password;
		message = `User ${req.body.password}'s password has been updated.`

		//Break out of the loop once the user that matches the uername provided  in the client postman is found
		break;
		}
		//if no user was found
		else{
			//Changes the message to be sent back as the response.
			message = "Users does not exist!";


		}
	}
//Send a response

res.send(message);


})










// This route expects to receive a DELETE request at the URI "/delete-user"
// This will remove the user from the array for deletion





app.delete("/delete-user", (req, res)=>{

	//Creates a variable to store the message to be sent back1 to the client/postman(response)
	let message;

	//Create a for loop that will loop through the elements of the user array
	for(let i=0; i < users.length; i++){

		//If the username provided in the client/Postman and username of the current element/object in the loop is the same
		if(users[i].username === req.body.username){

			//The splice method manipulates the array and removes the user object from the "users" array base on it's index.
		users.splice(users[i], 1);
		
		message = `User ${req.body.username}has been deleted.`

		//Break out of the loop once the user that matches the uername provided  in the client postman is found
		break;
		}
		//if no user was found
		else{
			//Changes the message to be sent back as the response.
			message = "Users does not exist!";


		}
	}
//Send a response

res.send(message);


})







//return a message to confirm that the server is running in ther terminal

app.listen(port, () => console.log(`
	Server is running at port: ${port}`));